# pavelko_fratisp_ufoIS

Advanced Software Design Course project NTNU

# Advanced Software Design - 1. assigment

This is a 1. assigment for Advanced Software Design. Modeling website is ufoIS. It's an information system for sport ufobal which is known olny in Czech republic. We focused on basic parts of the system. Model consists of tournaments, teams, players, matches and events. 

## Authors

Pavel Kohout - pavelko, 
Frantisek Pomkla - frantisp

## Website 

Link for the website: https://is.ufobal.cz/turnaje

## Screenshots

Screenshots are in the repo of the project.

## TODO for 2. assigment

- constraints: events (self-goal), regular expressions 
- add timestamp for more complex and detail informations about match
- anotation for data types
