/**
 */
package nizkov2021.ufois;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Match</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link nizkov2021.ufois.Match#getTeam_a <em>Team a</em>}</li>
 *   <li>{@link nizkov2021.ufois.Match#getTeam_b <em>Team b</em>}</li>
 *   <li>{@link nizkov2021.ufois.Match#getMatch_id <em>Match id</em>}</li>
 *   <li>{@link nizkov2021.ufois.Match#getScore <em>Score</em>}</li>
 *   <li>{@link nizkov2021.ufois.Match#getEvents <em>Events</em>}</li>
 * </ul>
 *
 * @see nizkov2021.ufois.UfoisPackage#getMatch()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='differentTeamsInMatch eventPlayerFromTeam notSameTeamOCL'"
 *        annotation="http://www.eclipse.org/acceleo/query/1.0 notSameTeamOCL='self.team_a != self.team_b'"
 * @generated
 */
public interface Match extends EObject {
	/**
	 * Returns the value of the '<em><b>Team a</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Team a</em>' reference.
	 * @see #setTeam_a(Team)
	 * @see nizkov2021.ufois.UfoisPackage#getMatch_Team_a()
	 * @model required="true"
	 * @generated
	 */
	Team getTeam_a();

	/**
	 * Sets the value of the '{@link nizkov2021.ufois.Match#getTeam_a <em>Team a</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Team a</em>' reference.
	 * @see #getTeam_a()
	 * @generated
	 */
	void setTeam_a(Team value);

	/**
	 * Returns the value of the '<em><b>Team b</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Team b</em>' reference.
	 * @see #setTeam_b(Team)
	 * @see nizkov2021.ufois.UfoisPackage#getMatch_Team_b()
	 * @model required="true"
	 * @generated
	 */
	Team getTeam_b();

	/**
	 * Sets the value of the '{@link nizkov2021.ufois.Match#getTeam_b <em>Team b</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Team b</em>' reference.
	 * @see #getTeam_b()
	 * @generated
	 */
	void setTeam_b(Team value);

	/**
	 * Returns the value of the '<em><b>Match id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Match id</em>' attribute.
	 * @see #setMatch_id(int)
	 * @see nizkov2021.ufois.UfoisPackage#getMatch_Match_id()
	 * @model
	 * @generated
	 */
	int getMatch_id();

	/**
	 * Sets the value of the '{@link nizkov2021.ufois.Match#getMatch_id <em>Match id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Match id</em>' attribute.
	 * @see #getMatch_id()
	 * @generated
	 */
	void setMatch_id(int value);

	/**
	 * Returns the value of the '<em><b>Score</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Score</em>' attribute.
	 * @see nizkov2021.ufois.UfoisPackage#getMatch_Score()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	String getScore();

	/**
	 * Returns the value of the '<em><b>Events</b></em>' containment reference list.
	 * The list contents are of type {@link nizkov2021.ufois.Event}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Events</em>' containment reference list.
	 * @see nizkov2021.ufois.UfoisPackage#getMatch_Events()
	 * @model containment="true"
	 * @generated
	 */
	EList<Event> getEvents();

} // Match
