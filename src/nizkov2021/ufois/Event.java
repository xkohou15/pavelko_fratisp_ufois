/**
 */
package nizkov2021.ufois;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link nizkov2021.ufois.Event#getType <em>Type</em>}</li>
 *   <li>{@link nizkov2021.ufois.Event#getAuthor <em>Author</em>}</li>
 *   <li>{@link nizkov2021.ufois.Event#getAssistant <em>Assistant</em>}</li>
 * </ul>
 *
 * @see nizkov2021.ufois.UfoisPackage#getEvent()
 * @model
 * @generated
 */
public interface Event extends EObject {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The literals are from the enumeration {@link nizkov2021.ufois.EEventType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see nizkov2021.ufois.EEventType
	 * @see #setType(EEventType)
	 * @see nizkov2021.ufois.UfoisPackage#getEvent_Type()
	 * @model
	 * @generated
	 */
	EEventType getType();

	/**
	 * Sets the value of the '{@link nizkov2021.ufois.Event#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see nizkov2021.ufois.EEventType
	 * @see #getType()
	 * @generated
	 */
	void setType(EEventType value);

	/**
	 * Returns the value of the '<em><b>Author</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link nizkov2021.ufois.Player#getEvents_author <em>Events author</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Author</em>' reference.
	 * @see #setAuthor(Player)
	 * @see nizkov2021.ufois.UfoisPackage#getEvent_Author()
	 * @see nizkov2021.ufois.Player#getEvents_author
	 * @model opposite="events_author" required="true"
	 * @generated
	 */
	Player getAuthor();

	/**
	 * Sets the value of the '{@link nizkov2021.ufois.Event#getAuthor <em>Author</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Author</em>' reference.
	 * @see #getAuthor()
	 * @generated
	 */
	void setAuthor(Player value);

	/**
	 * Returns the value of the '<em><b>Assistant</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link nizkov2021.ufois.Player#getEvents_assistant <em>Events assistant</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assistant</em>' reference.
	 * @see #setAssistant(Player)
	 * @see nizkov2021.ufois.UfoisPackage#getEvent_Assistant()
	 * @see nizkov2021.ufois.Player#getEvents_assistant
	 * @model opposite="events_assistant"
	 * @generated
	 */
	Player getAssistant();

	/**
	 * Sets the value of the '{@link nizkov2021.ufois.Event#getAssistant <em>Assistant</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Assistant</em>' reference.
	 * @see #getAssistant()
	 * @generated
	 */
	void setAssistant(Player value);

} // Event
