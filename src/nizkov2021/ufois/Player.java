/**
 */
package nizkov2021.ufois;

import java.time.LocalDate;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Player</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link nizkov2021.ufois.Player#getFirstname <em>Firstname</em>}</li>
 *   <li>{@link nizkov2021.ufois.Player#getSurname <em>Surname</em>}</li>
 *   <li>{@link nizkov2021.ufois.Player#getNickname <em>Nickname</em>}</li>
 *   <li>{@link nizkov2021.ufois.Player#getAge <em>Age</em>}</li>
 *   <li>{@link nizkov2021.ufois.Player#getTeam <em>Team</em>}</li>
 *   <li>{@link nizkov2021.ufois.Player#getSex <em>Sex</em>}</li>
 *   <li>{@link nizkov2021.ufois.Player#getEvents_author <em>Events author</em>}</li>
 *   <li>{@link nizkov2021.ufois.Player#getEvents_assistant <em>Events assistant</em>}</li>
 *   <li>{@link nizkov2021.ufois.Player#getBirthday <em>Birthday</em>}</li>
 * </ul>
 *
 * @see nizkov2021.ufois.UfoisPackage#getPlayer()
 * @model
 * @generated
 */
public interface Player extends EObject {
	/**
	 * Returns the value of the '<em><b>Firstname</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Firstname</em>' attribute.
	 * @see #setFirstname(String)
	 * @see nizkov2021.ufois.UfoisPackage#getPlayer_Firstname()
	 * @model
	 * @generated
	 */
	String getFirstname();

	/**
	 * Sets the value of the '{@link nizkov2021.ufois.Player#getFirstname <em>Firstname</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Firstname</em>' attribute.
	 * @see #getFirstname()
	 * @generated
	 */
	void setFirstname(String value);

	/**
	 * Returns the value of the '<em><b>Surname</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Surname</em>' attribute.
	 * @see #setSurname(String)
	 * @see nizkov2021.ufois.UfoisPackage#getPlayer_Surname()
	 * @model
	 * @generated
	 */
	String getSurname();

	/**
	 * Sets the value of the '{@link nizkov2021.ufois.Player#getSurname <em>Surname</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Surname</em>' attribute.
	 * @see #getSurname()
	 * @generated
	 */
	void setSurname(String value);

	/**
	 * Returns the value of the '<em><b>Nickname</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Nickname</em>' attribute.
	 * @see #setNickname(String)
	 * @see nizkov2021.ufois.UfoisPackage#getPlayer_Nickname()
	 * @model
	 * @generated
	 */
	String getNickname();

	/**
	 * Sets the value of the '{@link nizkov2021.ufois.Player#getNickname <em>Nickname</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Nickname</em>' attribute.
	 * @see #getNickname()
	 * @generated
	 */
	void setNickname(String value);

	/**
	 * Returns the value of the '<em><b>Age</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Age</em>' attribute.
	 * @see nizkov2021.ufois.UfoisPackage#getPlayer_Age()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 * @generated
	 */
	int getAge();

	/**
	 * Returns the value of the '<em><b>Team</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link nizkov2021.ufois.Team#getPlayers <em>Players</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Team</em>' container reference.
	 * @see #setTeam(Team)
	 * @see nizkov2021.ufois.UfoisPackage#getPlayer_Team()
	 * @see nizkov2021.ufois.Team#getPlayers
	 * @model opposite="players" transient="false"
	 * @generated
	 */
	Team getTeam();

	/**
	 * Sets the value of the '{@link nizkov2021.ufois.Player#getTeam <em>Team</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Team</em>' container reference.
	 * @see #getTeam()
	 * @generated
	 */
	void setTeam(Team value);

	/**
	 * Returns the value of the '<em><b>Sex</b></em>' attribute.
	 * The default value is <code>"man"</code>.
	 * The literals are from the enumeration {@link nizkov2021.ufois.ESex}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sex</em>' attribute.
	 * @see nizkov2021.ufois.ESex
	 * @see #setSex(ESex)
	 * @see nizkov2021.ufois.UfoisPackage#getPlayer_Sex()
	 * @model default="man"
	 * @generated
	 */
	ESex getSex();

	/**
	 * Sets the value of the '{@link nizkov2021.ufois.Player#getSex <em>Sex</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sex</em>' attribute.
	 * @see nizkov2021.ufois.ESex
	 * @see #getSex()
	 * @generated
	 */
	void setSex(ESex value);

	/**
	 * Returns the value of the '<em><b>Events author</b></em>' reference list.
	 * The list contents are of type {@link nizkov2021.ufois.Event}.
	 * It is bidirectional and its opposite is '{@link nizkov2021.ufois.Event#getAuthor <em>Author</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Events author</em>' reference list.
	 * @see nizkov2021.ufois.UfoisPackage#getPlayer_Events_author()
	 * @see nizkov2021.ufois.Event#getAuthor
	 * @model opposite="author"
	 * @generated
	 */
	EList<Event> getEvents_author();

	/**
	 * Returns the value of the '<em><b>Events assistant</b></em>' reference list.
	 * The list contents are of type {@link nizkov2021.ufois.Event}.
	 * It is bidirectional and its opposite is '{@link nizkov2021.ufois.Event#getAssistant <em>Assistant</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Events assistant</em>' reference list.
	 * @see nizkov2021.ufois.UfoisPackage#getPlayer_Events_assistant()
	 * @see nizkov2021.ufois.Event#getAssistant
	 * @model opposite="assistant"
	 * @generated
	 */
	EList<Event> getEvents_assistant();

	/**
	 * Returns the value of the '<em><b>Birthday</b></em>' attribute.
	 * The default value is <code>"java.time.LocalDate(2000-01-01)"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Birthday</em>' attribute.
	 * @see #setBirthday(LocalDate)
	 * @see nizkov2021.ufois.UfoisPackage#getPlayer_Birthday()
	 * @model default="java.time.LocalDate(2000-01-01)" dataType="nizkov2021.ufois.EBirthday"
	 * @generated
	 */
	LocalDate getBirthday();

	/**
	 * Sets the value of the '{@link nizkov2021.ufois.Player#getBirthday <em>Birthday</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Birthday</em>' attribute.
	 * @see #getBirthday()
	 * @generated
	 */
	void setBirthday(LocalDate value);

} // Player
