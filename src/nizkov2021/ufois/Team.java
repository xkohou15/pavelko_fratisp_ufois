/**
 */
package nizkov2021.ufois;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Team</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link nizkov2021.ufois.Team#getName <em>Name</em>}</li>
 *   <li>{@link nizkov2021.ufois.Team#getPlayers <em>Players</em>}</li>
 *   <li>{@link nizkov2021.ufois.Team#getTournament <em>Tournament</em>}</li>
 * </ul>
 *
 * @see nizkov2021.ufois.UfoisPackage#getTeam()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='oneWomanAtLeast'"
 * @generated
 */
public interface Team extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see nizkov2021.ufois.UfoisPackage#getTeam_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link nizkov2021.ufois.Team#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Players</b></em>' containment reference list.
	 * The list contents are of type {@link nizkov2021.ufois.Player}.
	 * It is bidirectional and its opposite is '{@link nizkov2021.ufois.Player#getTeam <em>Team</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Players</em>' containment reference list.
	 * @see nizkov2021.ufois.UfoisPackage#getTeam_Players()
	 * @see nizkov2021.ufois.Player#getTeam
	 * @model opposite="team" containment="true"
	 * @generated
	 */
	EList<Player> getPlayers();

	/**
	 * Returns the value of the '<em><b>Tournament</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link nizkov2021.ufois.Tournament#getTeams <em>Teams</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tournament</em>' container reference.
	 * @see #setTournament(Tournament)
	 * @see nizkov2021.ufois.UfoisPackage#getTeam_Tournament()
	 * @see nizkov2021.ufois.Tournament#getTeams
	 * @model opposite="teams" transient="false"
	 * @generated
	 */
	Tournament getTournament();

	/**
	 * Sets the value of the '{@link nizkov2021.ufois.Team#getTournament <em>Tournament</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Tournament</em>' container reference.
	 * @see #getTournament()
	 * @generated
	 */
	void setTournament(Tournament value);

} // Team
