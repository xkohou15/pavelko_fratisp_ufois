/**
 */
package nizkov2021.ufois.util;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import nizkov2021.ufois.*;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.EObjectValidator;

/**
 * <!-- begin-user-doc -->
 * The <b>Validator</b> for the model.
 * <!-- end-user-doc -->
 * @see nizkov2021.ufois.UfoisPackage
 * @generated
 */
public class UfoisValidator extends EObjectValidator {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final UfoisValidator INSTANCE = new UfoisValidator();

	/**
	 * A constant for the {@link org.eclipse.emf.common.util.Diagnostic#getSource() source} of diagnostic {@link org.eclipse.emf.common.util.Diagnostic#getCode() codes} from this package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.Diagnostic#getSource()
	 * @see org.eclipse.emf.common.util.Diagnostic#getCode()
	 * @generated
	 */
	public static final String DIAGNOSTIC_SOURCE = "nizkov2021.ufois";

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final int GENERATED_DIAGNOSTIC_CODE_COUNT = 0;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants in a derived class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final int DIAGNOSTIC_CODE_COUNT = GENERATED_DIAGNOSTIC_CODE_COUNT;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UfoisValidator() {
		super();
	}

	/**
	 * Returns the package of this validator switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EPackage getEPackage() {
	  return UfoisPackage.eINSTANCE;
	}

	/**
	 * Calls <code>validateXXX</code> for the corresponding classifier of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean validate(int classifierID, Object value, DiagnosticChain diagnostics, Map<Object, Object> context) {
		switch (classifierID) {
			case UfoisPackage.TOURNAMENT:
				return validateTournament((Tournament)value, diagnostics, context);
			case UfoisPackage.PLAYER:
				return validatePlayer((Player)value, diagnostics, context);
			case UfoisPackage.TEAM:
				return validateTeam((Team)value, diagnostics, context);
			case UfoisPackage.MATCH:
				return validateMatch((Match)value, diagnostics, context);
			case UfoisPackage.UFO_IS:
				return validateUfoIS((UfoIS)value, diagnostics, context);
			case UfoisPackage.EVENT:
				return validateEvent((Event)value, diagnostics, context);
			case UfoisPackage.EEVENT_TYPE:
				return validateEEventType((EEventType)value, diagnostics, context);
			case UfoisPackage.ESEX:
				return validateESex((ESex)value, diagnostics, context);
			case UfoisPackage.EBIRTHDAY:
				return validateEBirthday((LocalDate)value, diagnostics, context);
			default:
				return true;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTournament(Tournament tournament, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(tournament, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePlayer(Player player, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(player, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTeam(Team team, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(team, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(team, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(team, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(team, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(team, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(team, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(team, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(team, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(team, diagnostics, context);
		if (result || diagnostics != null) result &= validateTeam_oneWomanAtLeast(team, diagnostics, context);
		return result;
	}

	/**
	 * Validates the oneWomanAtLeast constraint of '<em>Team</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean validateTeam_oneWomanAtLeast(Team team, DiagnosticChain diagnostics, Map<Object, Object> context) {
		int women_cnt = 0;
		for (Player p : team.getPlayers()) {
			if (p.getSex() == ESex.WOMAN) {
				women_cnt++;
			}
		}
		if (women_cnt < 1) {
			if (diagnostics != null) {
				diagnostics.add
					(createDiagnostic
						(Diagnostic.ERROR,
						 DIAGNOSTIC_SOURCE,
						 0,
						 "_UI_GenericConstraint_diagnostic",
						 new Object[] { "oneWomanAtLeast", getObjectLabel(team, context) },
						 new Object[] { team },
						 context));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMatch(Match match, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(match, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms(match, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(match, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(match, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired(match, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(match, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(match, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(match, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(match, diagnostics, context);
		if (result || diagnostics != null) result &= validateMatch_differentTeamsInMatch(match, diagnostics, context);
		if (result || diagnostics != null) result &= validateMatch_eventPlayerFromTeam(match, diagnostics, context);
		if (result || diagnostics != null) result &= validateMatch_notSameTeamOCL(match, diagnostics, context);
		return result;
	}

	/**
	 * Validates the differentTeamsInMatch constraint of '<em>Match</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean validateMatch_differentTeamsInMatch(Match match, DiagnosticChain diagnostics, Map<Object, Object> context) {
		// Teams must not be same 
		if (match.getTeam_a().equals(match.getTeam_b())) {
			if (diagnostics != null) {
				diagnostics.add
					(createDiagnostic
						(Diagnostic.ERROR,
						 DIAGNOSTIC_SOURCE,
						 0,
						 "_UI_GenericConstraint_diagnostic",
						 new Object[] { "differentTeamsInMatch", getObjectLabel(match, context) },
						 new Object[] { match },
						 context));
			}
			return false;
		}
		return true;
	}

	/**
	 * Validates the eventPlayerFromTeam constraint of '<em>Match</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean validateMatch_eventPlayerFromTeam(Match match, DiagnosticChain diagnostics, Map<Object, Object> context) {
		List<Event> events = match.getEvents();
		if (events == null) {
			return true;
		}
		// prepare flags
		boolean author_flag = false;
		boolean assistant_flag = false;
			
		// iterate over events and check whether players correspond to teams playing the match
		for(Event e : events) {
			// author is allocated in every event (red card / goal)
			Team author_team = e.getAuthor().getTeam();
			if(author_team != match.getTeam_a() && author_team != match.getTeam_b()) {
				author_flag = true;
				break;
			}
			// assistant player is not from teams playing the match
			Player assistant = e.getAssistant();
			if (assistant != null) {
				if(		e.getType() == EEventType.GOAL   &&
						assistant.getTeam() != match.getTeam_a() && 
						assistant.getTeam() != match.getTeam_b()) {
					assistant_flag = true;
					break;
				}
			}
		}
		
		if (author_flag || assistant_flag) {
			if (diagnostics != null) {
				diagnostics.add
					(createDiagnostic
						(Diagnostic.ERROR,
						 DIAGNOSTIC_SOURCE,
						 0,
						 "_UI_GenericConstraint_diagnostic",
						 new Object[] { "eventPlayerFromTeam", getObjectLabel(match, context) },
						 new Object[] { match },
						 context));
			}
			return false;
		}
		return true;
	}

	/**
	 * The cached validation expression for the notSameTeamOCL constraint of '<em>Match</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String MATCH__NOT_SAME_TEAM_OCL__EEXPRESSION = "self.team_a != self.team_b";

	/**
	 * Validates the notSameTeamOCL constraint of '<em>Match</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMatch_notSameTeamOCL(Match match, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(UfoisPackage.Literals.MATCH,
				 match,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/acceleo/query/1.0",
				 "notSameTeamOCL",
				 MATCH__NOT_SAME_TEAM_OCL__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateUfoIS(UfoIS ufoIS, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(ufoIS, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEvent(Event event, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(event, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEEventType(EEventType eEventType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateESex(ESex eSex, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEBirthday(LocalDate eBirthday, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * Returns the resource locator that will be used to fetch messages for this validator's diagnostics.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		// TODO
		// Specialize this to return a resource locator for messages specific to this validator.
		// Ensure that you remove @generated or mark it @generated NOT
		return super.getResourceLocator();
	}

} //UfoisValidator
