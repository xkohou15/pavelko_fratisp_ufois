/**
 */
package nizkov2021.ufois;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Tournament</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link nizkov2021.ufois.Tournament#getTeams <em>Teams</em>}</li>
 *   <li>{@link nizkov2021.ufois.Tournament#getMatches <em>Matches</em>}</li>
 *   <li>{@link nizkov2021.ufois.Tournament#getName <em>Name</em>}</li>
 * </ul>
 *
 * @see nizkov2021.ufois.UfoisPackage#getTournament()
 * @model
 * @generated
 */
public interface Tournament extends EObject {
	/**
	 * Returns the value of the '<em><b>Teams</b></em>' containment reference list.
	 * The list contents are of type {@link nizkov2021.ufois.Team}.
	 * It is bidirectional and its opposite is '{@link nizkov2021.ufois.Team#getTournament <em>Tournament</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Teams</em>' containment reference list.
	 * @see nizkov2021.ufois.UfoisPackage#getTournament_Teams()
	 * @see nizkov2021.ufois.Team#getTournament
	 * @model opposite="tournament" containment="true"
	 * @generated
	 */
	EList<Team> getTeams();

	/**
	 * Returns the value of the '<em><b>Matches</b></em>' containment reference list.
	 * The list contents are of type {@link nizkov2021.ufois.Match}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Matches</em>' containment reference list.
	 * @see nizkov2021.ufois.UfoisPackage#getTournament_Matches()
	 * @model containment="true"
	 * @generated
	 */
	EList<Match> getMatches();

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see nizkov2021.ufois.UfoisPackage#getTournament_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link nizkov2021.ufois.Tournament#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model authorRequired="true"
	 * @generated
	 */
	void addEventToMatch(Match match, EEventType eventType, Player author, Player assistant);

} // Tournament
