/**
 */
package nizkov2021.ufois.impl;

import java.time.LocalDate;
import java.time.Period;
import java.util.Collection;

import nizkov2021.ufois.ESex;
import nizkov2021.ufois.Event;
import nizkov2021.ufois.Player;
import nizkov2021.ufois.Team;
import nizkov2021.ufois.UfoisFactory;
import nizkov2021.ufois.UfoisPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Player</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link nizkov2021.ufois.impl.PlayerImpl#getFirstname <em>Firstname</em>}</li>
 *   <li>{@link nizkov2021.ufois.impl.PlayerImpl#getSurname <em>Surname</em>}</li>
 *   <li>{@link nizkov2021.ufois.impl.PlayerImpl#getNickname <em>Nickname</em>}</li>
 *   <li>{@link nizkov2021.ufois.impl.PlayerImpl#getAge <em>Age</em>}</li>
 *   <li>{@link nizkov2021.ufois.impl.PlayerImpl#getTeam <em>Team</em>}</li>
 *   <li>{@link nizkov2021.ufois.impl.PlayerImpl#getSex <em>Sex</em>}</li>
 *   <li>{@link nizkov2021.ufois.impl.PlayerImpl#getEvents_author <em>Events author</em>}</li>
 *   <li>{@link nizkov2021.ufois.impl.PlayerImpl#getEvents_assistant <em>Events assistant</em>}</li>
 *   <li>{@link nizkov2021.ufois.impl.PlayerImpl#getBirthday <em>Birthday</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PlayerImpl extends MinimalEObjectImpl.Container implements Player {
	/**
	 * The default value of the '{@link #getFirstname() <em>Firstname</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFirstname()
	 * @generated
	 * @ordered
	 */
	protected static final String FIRSTNAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFirstname() <em>Firstname</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFirstname()
	 * @generated
	 * @ordered
	 */
	protected String firstname = FIRSTNAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getSurname() <em>Surname</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSurname()
	 * @generated
	 * @ordered
	 */
	protected static final String SURNAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSurname() <em>Surname</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSurname()
	 * @generated
	 * @ordered
	 */
	protected String surname = SURNAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getNickname() <em>Nickname</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNickname()
	 * @generated
	 * @ordered
	 */
	protected static final String NICKNAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getNickname() <em>Nickname</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNickname()
	 * @generated
	 * @ordered
	 */
	protected String nickname = NICKNAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getAge() <em>Age</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAge()
	 * @generated
	 * @ordered
	 */
	protected static final int AGE_EDEFAULT = 0;

	/**
	 * The default value of the '{@link #getSex() <em>Sex</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSex()
	 * @generated
	 * @ordered
	 */
	protected static final ESex SEX_EDEFAULT = ESex.MAN;

	/**
	 * The cached value of the '{@link #getSex() <em>Sex</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSex()
	 * @generated
	 * @ordered
	 */
	protected ESex sex = SEX_EDEFAULT;

	/**
	 * The cached value of the '{@link #getEvents_author() <em>Events author</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEvents_author()
	 * @generated
	 * @ordered
	 */
	protected EList<Event> events_author;

	/**
	 * The cached value of the '{@link #getEvents_assistant() <em>Events assistant</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEvents_assistant()
	 * @generated
	 * @ordered
	 */
	protected EList<Event> events_assistant;

	/**
	 * The default value of the '{@link #getBirthday() <em>Birthday</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBirthday()
	 * @generated
	 * @ordered
	 */
	protected static final LocalDate BIRTHDAY_EDEFAULT = (LocalDate)UfoisFactory.eINSTANCE.createFromString(UfoisPackage.eINSTANCE.getEBirthday(), "java.time.LocalDate(2000-01-01)");

	/**
	 * The cached value of the '{@link #getBirthday() <em>Birthday</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBirthday()
	 * @generated
	 * @ordered
	 */
	protected LocalDate birthday = BIRTHDAY_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PlayerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UfoisPackage.Literals.PLAYER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getFirstname() {
		return firstname;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFirstname(String newFirstname) {
		String oldFirstname = firstname;
		firstname = newFirstname;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UfoisPackage.PLAYER__FIRSTNAME, oldFirstname, firstname));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSurname() {
		return surname;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSurname(String newSurname) {
		String oldSurname = surname;
		surname = newSurname;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UfoisPackage.PLAYER__SURNAME, oldSurname, surname));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getNickname() {
		return nickname;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNickname(String newNickname) {
		String oldNickname = nickname;
		nickname = newNickname;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UfoisPackage.PLAYER__NICKNAME, oldNickname, nickname));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int getAge() {
		if (this.birthday != null) {
			return Period.between(birthday, LocalDate.now()).getYears();
		} else {
			return 0;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Team getTeam() {
		if (eContainerFeatureID() != UfoisPackage.PLAYER__TEAM) return null;
		return (Team)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTeam(Team newTeam, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newTeam, UfoisPackage.PLAYER__TEAM, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTeam(Team newTeam) {
		if (newTeam != eInternalContainer() || (eContainerFeatureID() != UfoisPackage.PLAYER__TEAM && newTeam != null)) {
			if (EcoreUtil.isAncestor(this, newTeam))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newTeam != null)
				msgs = ((InternalEObject)newTeam).eInverseAdd(this, UfoisPackage.TEAM__PLAYERS, Team.class, msgs);
			msgs = basicSetTeam(newTeam, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UfoisPackage.PLAYER__TEAM, newTeam, newTeam));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ESex getSex() {
		return sex;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSex(ESex newSex) {
		ESex oldSex = sex;
		sex = newSex == null ? SEX_EDEFAULT : newSex;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UfoisPackage.PLAYER__SEX, oldSex, sex));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Event> getEvents_author() {
		if (events_author == null) {
			events_author = new EObjectWithInverseResolvingEList<Event>(Event.class, this, UfoisPackage.PLAYER__EVENTS_AUTHOR, UfoisPackage.EVENT__AUTHOR);
		}
		return events_author;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Event> getEvents_assistant() {
		if (events_assistant == null) {
			events_assistant = new EObjectWithInverseResolvingEList<Event>(Event.class, this, UfoisPackage.PLAYER__EVENTS_ASSISTANT, UfoisPackage.EVENT__ASSISTANT);
		}
		return events_assistant;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LocalDate getBirthday() {
		return birthday;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBirthday(LocalDate newBirthday) {
		LocalDate oldBirthday = birthday;
		birthday = newBirthday;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UfoisPackage.PLAYER__BIRTHDAY, oldBirthday, birthday));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case UfoisPackage.PLAYER__TEAM:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetTeam((Team)otherEnd, msgs);
			case UfoisPackage.PLAYER__EVENTS_AUTHOR:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getEvents_author()).basicAdd(otherEnd, msgs);
			case UfoisPackage.PLAYER__EVENTS_ASSISTANT:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getEvents_assistant()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case UfoisPackage.PLAYER__TEAM:
				return basicSetTeam(null, msgs);
			case UfoisPackage.PLAYER__EVENTS_AUTHOR:
				return ((InternalEList<?>)getEvents_author()).basicRemove(otherEnd, msgs);
			case UfoisPackage.PLAYER__EVENTS_ASSISTANT:
				return ((InternalEList<?>)getEvents_assistant()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case UfoisPackage.PLAYER__TEAM:
				return eInternalContainer().eInverseRemove(this, UfoisPackage.TEAM__PLAYERS, Team.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case UfoisPackage.PLAYER__FIRSTNAME:
				return getFirstname();
			case UfoisPackage.PLAYER__SURNAME:
				return getSurname();
			case UfoisPackage.PLAYER__NICKNAME:
				return getNickname();
			case UfoisPackage.PLAYER__AGE:
				return getAge();
			case UfoisPackage.PLAYER__TEAM:
				return getTeam();
			case UfoisPackage.PLAYER__SEX:
				return getSex();
			case UfoisPackage.PLAYER__EVENTS_AUTHOR:
				return getEvents_author();
			case UfoisPackage.PLAYER__EVENTS_ASSISTANT:
				return getEvents_assistant();
			case UfoisPackage.PLAYER__BIRTHDAY:
				return getBirthday();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case UfoisPackage.PLAYER__FIRSTNAME:
				setFirstname((String)newValue);
				return;
			case UfoisPackage.PLAYER__SURNAME:
				setSurname((String)newValue);
				return;
			case UfoisPackage.PLAYER__NICKNAME:
				setNickname((String)newValue);
				return;
			case UfoisPackage.PLAYER__TEAM:
				setTeam((Team)newValue);
				return;
			case UfoisPackage.PLAYER__SEX:
				setSex((ESex)newValue);
				return;
			case UfoisPackage.PLAYER__EVENTS_AUTHOR:
				getEvents_author().clear();
				getEvents_author().addAll((Collection<? extends Event>)newValue);
				return;
			case UfoisPackage.PLAYER__EVENTS_ASSISTANT:
				getEvents_assistant().clear();
				getEvents_assistant().addAll((Collection<? extends Event>)newValue);
				return;
			case UfoisPackage.PLAYER__BIRTHDAY:
				setBirthday((LocalDate)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case UfoisPackage.PLAYER__FIRSTNAME:
				setFirstname(FIRSTNAME_EDEFAULT);
				return;
			case UfoisPackage.PLAYER__SURNAME:
				setSurname(SURNAME_EDEFAULT);
				return;
			case UfoisPackage.PLAYER__NICKNAME:
				setNickname(NICKNAME_EDEFAULT);
				return;
			case UfoisPackage.PLAYER__TEAM:
				setTeam((Team)null);
				return;
			case UfoisPackage.PLAYER__SEX:
				setSex(SEX_EDEFAULT);
				return;
			case UfoisPackage.PLAYER__EVENTS_AUTHOR:
				getEvents_author().clear();
				return;
			case UfoisPackage.PLAYER__EVENTS_ASSISTANT:
				getEvents_assistant().clear();
				return;
			case UfoisPackage.PLAYER__BIRTHDAY:
				setBirthday(BIRTHDAY_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case UfoisPackage.PLAYER__FIRSTNAME:
				return FIRSTNAME_EDEFAULT == null ? firstname != null : !FIRSTNAME_EDEFAULT.equals(firstname);
			case UfoisPackage.PLAYER__SURNAME:
				return SURNAME_EDEFAULT == null ? surname != null : !SURNAME_EDEFAULT.equals(surname);
			case UfoisPackage.PLAYER__NICKNAME:
				return NICKNAME_EDEFAULT == null ? nickname != null : !NICKNAME_EDEFAULT.equals(nickname);
			case UfoisPackage.PLAYER__AGE:
				return getAge() != AGE_EDEFAULT;
			case UfoisPackage.PLAYER__TEAM:
				return getTeam() != null;
			case UfoisPackage.PLAYER__SEX:
				return sex != SEX_EDEFAULT;
			case UfoisPackage.PLAYER__EVENTS_AUTHOR:
				return events_author != null && !events_author.isEmpty();
			case UfoisPackage.PLAYER__EVENTS_ASSISTANT:
				return events_assistant != null && !events_assistant.isEmpty();
			case UfoisPackage.PLAYER__BIRTHDAY:
				return BIRTHDAY_EDEFAULT == null ? birthday != null : !BIRTHDAY_EDEFAULT.equals(birthday);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (firstname: ");
		result.append(firstname);
		result.append(", surname: ");
		result.append(surname);
		result.append(", nickname: ");
		result.append(nickname);
		result.append(", sex: ");
		result.append(sex);
		result.append(", birthday: ");
		result.append(birthday);
		result.append(')');
		return result.toString();
	}

} //PlayerImpl
