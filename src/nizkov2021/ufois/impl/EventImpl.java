/**
 */
package nizkov2021.ufois.impl;

import nizkov2021.ufois.EEventType;
import nizkov2021.ufois.Event;
import nizkov2021.ufois.Player;
import nizkov2021.ufois.UfoisPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Event</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link nizkov2021.ufois.impl.EventImpl#getType <em>Type</em>}</li>
 *   <li>{@link nizkov2021.ufois.impl.EventImpl#getAuthor <em>Author</em>}</li>
 *   <li>{@link nizkov2021.ufois.impl.EventImpl#getAssistant <em>Assistant</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EventImpl extends MinimalEObjectImpl.Container implements Event {
	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final EEventType TYPE_EDEFAULT = EEventType.GOAL;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected EEventType type = TYPE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getAuthor() <em>Author</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAuthor()
	 * @generated
	 * @ordered
	 */
	protected Player author;

	/**
	 * The cached value of the '{@link #getAssistant() <em>Assistant</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssistant()
	 * @generated
	 * @ordered
	 */
	protected Player assistant;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EventImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UfoisPackage.Literals.EVENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEventType getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(EEventType newType) {
		EEventType oldType = type;
		type = newType == null ? TYPE_EDEFAULT : newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UfoisPackage.EVENT__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Player getAuthor() {
		if (author != null && author.eIsProxy()) {
			InternalEObject oldAuthor = (InternalEObject)author;
			author = (Player)eResolveProxy(oldAuthor);
			if (author != oldAuthor) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, UfoisPackage.EVENT__AUTHOR, oldAuthor, author));
			}
		}
		return author;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Player basicGetAuthor() {
		return author;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAuthor(Player newAuthor, NotificationChain msgs) {
		Player oldAuthor = author;
		author = newAuthor;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, UfoisPackage.EVENT__AUTHOR, oldAuthor, newAuthor);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAuthor(Player newAuthor) {
		if (newAuthor != author) {
			NotificationChain msgs = null;
			if (author != null)
				msgs = ((InternalEObject)author).eInverseRemove(this, UfoisPackage.PLAYER__EVENTS_AUTHOR, Player.class, msgs);
			if (newAuthor != null)
				msgs = ((InternalEObject)newAuthor).eInverseAdd(this, UfoisPackage.PLAYER__EVENTS_AUTHOR, Player.class, msgs);
			msgs = basicSetAuthor(newAuthor, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UfoisPackage.EVENT__AUTHOR, newAuthor, newAuthor));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Player getAssistant() {
		if (assistant != null && assistant.eIsProxy()) {
			InternalEObject oldAssistant = (InternalEObject)assistant;
			assistant = (Player)eResolveProxy(oldAssistant);
			if (assistant != oldAssistant) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, UfoisPackage.EVENT__ASSISTANT, oldAssistant, assistant));
			}
		}
		return assistant;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Player basicGetAssistant() {
		return assistant;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAssistant(Player newAssistant, NotificationChain msgs) {
		Player oldAssistant = assistant;
		assistant = newAssistant;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, UfoisPackage.EVENT__ASSISTANT, oldAssistant, newAssistant);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssistant(Player newAssistant) {
		if (newAssistant != assistant) {
			NotificationChain msgs = null;
			if (assistant != null)
				msgs = ((InternalEObject)assistant).eInverseRemove(this, UfoisPackage.PLAYER__EVENTS_ASSISTANT, Player.class, msgs);
			if (newAssistant != null)
				msgs = ((InternalEObject)newAssistant).eInverseAdd(this, UfoisPackage.PLAYER__EVENTS_ASSISTANT, Player.class, msgs);
			msgs = basicSetAssistant(newAssistant, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UfoisPackage.EVENT__ASSISTANT, newAssistant, newAssistant));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case UfoisPackage.EVENT__AUTHOR:
				if (author != null)
					msgs = ((InternalEObject)author).eInverseRemove(this, UfoisPackage.PLAYER__EVENTS_AUTHOR, Player.class, msgs);
				return basicSetAuthor((Player)otherEnd, msgs);
			case UfoisPackage.EVENT__ASSISTANT:
				if (assistant != null)
					msgs = ((InternalEObject)assistant).eInverseRemove(this, UfoisPackage.PLAYER__EVENTS_ASSISTANT, Player.class, msgs);
				return basicSetAssistant((Player)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case UfoisPackage.EVENT__AUTHOR:
				return basicSetAuthor(null, msgs);
			case UfoisPackage.EVENT__ASSISTANT:
				return basicSetAssistant(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case UfoisPackage.EVENT__TYPE:
				return getType();
			case UfoisPackage.EVENT__AUTHOR:
				if (resolve) return getAuthor();
				return basicGetAuthor();
			case UfoisPackage.EVENT__ASSISTANT:
				if (resolve) return getAssistant();
				return basicGetAssistant();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case UfoisPackage.EVENT__TYPE:
				setType((EEventType)newValue);
				return;
			case UfoisPackage.EVENT__AUTHOR:
				setAuthor((Player)newValue);
				return;
			case UfoisPackage.EVENT__ASSISTANT:
				setAssistant((Player)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case UfoisPackage.EVENT__TYPE:
				setType(TYPE_EDEFAULT);
				return;
			case UfoisPackage.EVENT__AUTHOR:
				setAuthor((Player)null);
				return;
			case UfoisPackage.EVENT__ASSISTANT:
				setAssistant((Player)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case UfoisPackage.EVENT__TYPE:
				return type != TYPE_EDEFAULT;
			case UfoisPackage.EVENT__AUTHOR:
				return author != null;
			case UfoisPackage.EVENT__ASSISTANT:
				return assistant != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (type: ");
		result.append(type);
		result.append(')');
		return result.toString();
	}

} //EventImpl
