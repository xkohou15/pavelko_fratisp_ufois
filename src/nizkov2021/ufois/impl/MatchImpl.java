/**
 */
package nizkov2021.ufois.impl;

import java.util.Collection;

import nizkov2021.ufois.EEventType;
import nizkov2021.ufois.Event;
import nizkov2021.ufois.Match;
import nizkov2021.ufois.Team;
import nizkov2021.ufois.UfoisPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Match</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link nizkov2021.ufois.impl.MatchImpl#getTeam_a <em>Team a</em>}</li>
 *   <li>{@link nizkov2021.ufois.impl.MatchImpl#getTeam_b <em>Team b</em>}</li>
 *   <li>{@link nizkov2021.ufois.impl.MatchImpl#getMatch_id <em>Match id</em>}</li>
 *   <li>{@link nizkov2021.ufois.impl.MatchImpl#getScore <em>Score</em>}</li>
 *   <li>{@link nizkov2021.ufois.impl.MatchImpl#getEvents <em>Events</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MatchImpl extends MinimalEObjectImpl.Container implements Match {
	/**
	 * The cached value of the '{@link #getTeam_a() <em>Team a</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTeam_a()
	 * @generated
	 * @ordered
	 */
	protected Team team_a;

	/**
	 * The cached value of the '{@link #getTeam_b() <em>Team b</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTeam_b()
	 * @generated
	 * @ordered
	 */
	protected Team team_b;

	/**
	 * The default value of the '{@link #getMatch_id() <em>Match id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMatch_id()
	 * @generated
	 * @ordered
	 */
	protected static final int MATCH_ID_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getMatch_id() <em>Match id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMatch_id()
	 * @generated
	 * @ordered
	 */
	protected int match_id = MATCH_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getScore() <em>Score</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScore()
	 * @generated
	 * @ordered
	 */
	protected static final String SCORE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getEvents() <em>Events</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEvents()
	 * @generated
	 * @ordered
	 */
	protected EList<Event> events;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MatchImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UfoisPackage.Literals.MATCH;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Team getTeam_a() {
		if (team_a != null && team_a.eIsProxy()) {
			InternalEObject oldTeam_a = (InternalEObject)team_a;
			team_a = (Team)eResolveProxy(oldTeam_a);
			if (team_a != oldTeam_a) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, UfoisPackage.MATCH__TEAM_A, oldTeam_a, team_a));
			}
		}
		return team_a;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Team basicGetTeam_a() {
		return team_a;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTeam_a(Team newTeam_a) {
		Team oldTeam_a = team_a;
		team_a = newTeam_a;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UfoisPackage.MATCH__TEAM_A, oldTeam_a, team_a));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Team getTeam_b() {
		if (team_b != null && team_b.eIsProxy()) {
			InternalEObject oldTeam_b = (InternalEObject)team_b;
			team_b = (Team)eResolveProxy(oldTeam_b);
			if (team_b != oldTeam_b) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, UfoisPackage.MATCH__TEAM_B, oldTeam_b, team_b));
			}
		}
		return team_b;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Team basicGetTeam_b() {
		return team_b;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTeam_b(Team newTeam_b) {
		Team oldTeam_b = team_b;
		team_b = newTeam_b;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UfoisPackage.MATCH__TEAM_B, oldTeam_b, team_b));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getMatch_id() {
		return match_id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMatch_id(int newMatch_id) {
		int oldMatch_id = match_id;
		match_id = newMatch_id;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UfoisPackage.MATCH__MATCH_ID, oldMatch_id, match_id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public String getScore() {
		if (this.events == null) {
			return "-:-";
		}
		
		int score_a = 0;
		int score_b = 0;
		for(final Event e : this.events) {
			if(e.getType() == EEventType.GOAL) {
				if(e.getAuthor().getTeam() == this.team_a) {
					score_a++;
				} else if(e.getAuthor().getTeam() == this.team_b) {
					score_b++;
				}	
				// corner case is tested in constrain -> not in either A nor B team
			}
		}
		return Integer.toString(score_a) + ":" + Integer.toString(score_b);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Event> getEvents() {
		if (events == null) {
			events = new EObjectContainmentEList<Event>(Event.class, this, UfoisPackage.MATCH__EVENTS);
		}
		return events;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case UfoisPackage.MATCH__EVENTS:
				return ((InternalEList<?>)getEvents()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case UfoisPackage.MATCH__TEAM_A:
				if (resolve) return getTeam_a();
				return basicGetTeam_a();
			case UfoisPackage.MATCH__TEAM_B:
				if (resolve) return getTeam_b();
				return basicGetTeam_b();
			case UfoisPackage.MATCH__MATCH_ID:
				return getMatch_id();
			case UfoisPackage.MATCH__SCORE:
				return getScore();
			case UfoisPackage.MATCH__EVENTS:
				return getEvents();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case UfoisPackage.MATCH__TEAM_A:
				setTeam_a((Team)newValue);
				return;
			case UfoisPackage.MATCH__TEAM_B:
				setTeam_b((Team)newValue);
				return;
			case UfoisPackage.MATCH__MATCH_ID:
				setMatch_id((Integer)newValue);
				return;
			case UfoisPackage.MATCH__EVENTS:
				getEvents().clear();
				getEvents().addAll((Collection<? extends Event>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case UfoisPackage.MATCH__TEAM_A:
				setTeam_a((Team)null);
				return;
			case UfoisPackage.MATCH__TEAM_B:
				setTeam_b((Team)null);
				return;
			case UfoisPackage.MATCH__MATCH_ID:
				setMatch_id(MATCH_ID_EDEFAULT);
				return;
			case UfoisPackage.MATCH__EVENTS:
				getEvents().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case UfoisPackage.MATCH__TEAM_A:
				return team_a != null;
			case UfoisPackage.MATCH__TEAM_B:
				return team_b != null;
			case UfoisPackage.MATCH__MATCH_ID:
				return match_id != MATCH_ID_EDEFAULT;
			case UfoisPackage.MATCH__SCORE:
				return SCORE_EDEFAULT == null ? getScore() != null : !SCORE_EDEFAULT.equals(getScore());
			case UfoisPackage.MATCH__EVENTS:
				return events != null && !events.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (match_id: ");
		result.append(match_id);
		result.append(')');
		return result.toString();
	}

} //MatchImpl
