/**
 */
package nizkov2021.ufois;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see nizkov2021.ufois.UfoisFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore validationDelegates='http://www.eclipse.org/acceleo/query/1.0'"
 * @generated
 */
public interface UfoisPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "ufois";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "platform/plugin/pavelko_frantisp_ufoIS/model/ufois.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "ufois";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	UfoisPackage eINSTANCE = nizkov2021.ufois.impl.UfoisPackageImpl.init();

	/**
	 * The meta object id for the '{@link nizkov2021.ufois.impl.TournamentImpl <em>Tournament</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nizkov2021.ufois.impl.TournamentImpl
	 * @see nizkov2021.ufois.impl.UfoisPackageImpl#getTournament()
	 * @generated
	 */
	int TOURNAMENT = 0;

	/**
	 * The feature id for the '<em><b>Teams</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOURNAMENT__TEAMS = 0;

	/**
	 * The feature id for the '<em><b>Matches</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOURNAMENT__MATCHES = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOURNAMENT__NAME = 2;

	/**
	 * The number of structural features of the '<em>Tournament</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOURNAMENT_FEATURE_COUNT = 3;

	/**
	 * The operation id for the '<em>Add Event To Match</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOURNAMENT___ADD_EVENT_TO_MATCH__MATCH_EEVENTTYPE_PLAYER_PLAYER = 0;

	/**
	 * The number of operations of the '<em>Tournament</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOURNAMENT_OPERATION_COUNT = 1;

	/**
	 * The meta object id for the '{@link nizkov2021.ufois.impl.PlayerImpl <em>Player</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nizkov2021.ufois.impl.PlayerImpl
	 * @see nizkov2021.ufois.impl.UfoisPackageImpl#getPlayer()
	 * @generated
	 */
	int PLAYER = 1;

	/**
	 * The feature id for the '<em><b>Firstname</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLAYER__FIRSTNAME = 0;

	/**
	 * The feature id for the '<em><b>Surname</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLAYER__SURNAME = 1;

	/**
	 * The feature id for the '<em><b>Nickname</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLAYER__NICKNAME = 2;

	/**
	 * The feature id for the '<em><b>Age</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLAYER__AGE = 3;

	/**
	 * The feature id for the '<em><b>Team</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLAYER__TEAM = 4;

	/**
	 * The feature id for the '<em><b>Sex</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLAYER__SEX = 5;

	/**
	 * The feature id for the '<em><b>Events author</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLAYER__EVENTS_AUTHOR = 6;

	/**
	 * The feature id for the '<em><b>Events assistant</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLAYER__EVENTS_ASSISTANT = 7;

	/**
	 * The feature id for the '<em><b>Birthday</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLAYER__BIRTHDAY = 8;

	/**
	 * The number of structural features of the '<em>Player</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLAYER_FEATURE_COUNT = 9;

	/**
	 * The number of operations of the '<em>Player</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLAYER_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link nizkov2021.ufois.impl.TeamImpl <em>Team</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nizkov2021.ufois.impl.TeamImpl
	 * @see nizkov2021.ufois.impl.UfoisPackageImpl#getTeam()
	 * @generated
	 */
	int TEAM = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEAM__NAME = 0;

	/**
	 * The feature id for the '<em><b>Players</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEAM__PLAYERS = 1;

	/**
	 * The feature id for the '<em><b>Tournament</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEAM__TOURNAMENT = 2;

	/**
	 * The number of structural features of the '<em>Team</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEAM_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Team</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEAM_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link nizkov2021.ufois.impl.MatchImpl <em>Match</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nizkov2021.ufois.impl.MatchImpl
	 * @see nizkov2021.ufois.impl.UfoisPackageImpl#getMatch()
	 * @generated
	 */
	int MATCH = 3;

	/**
	 * The feature id for the '<em><b>Team a</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATCH__TEAM_A = 0;

	/**
	 * The feature id for the '<em><b>Team b</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATCH__TEAM_B = 1;

	/**
	 * The feature id for the '<em><b>Match id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATCH__MATCH_ID = 2;

	/**
	 * The feature id for the '<em><b>Score</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATCH__SCORE = 3;

	/**
	 * The feature id for the '<em><b>Events</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATCH__EVENTS = 4;

	/**
	 * The number of structural features of the '<em>Match</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATCH_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>Match</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATCH_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link nizkov2021.ufois.impl.UfoISImpl <em>Ufo IS</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nizkov2021.ufois.impl.UfoISImpl
	 * @see nizkov2021.ufois.impl.UfoisPackageImpl#getUfoIS()
	 * @generated
	 */
	int UFO_IS = 4;

	/**
	 * The feature id for the '<em><b>Tournaments</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UFO_IS__TOURNAMENTS = 0;

	/**
	 * The number of structural features of the '<em>Ufo IS</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UFO_IS_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Ufo IS</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UFO_IS_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link nizkov2021.ufois.impl.EventImpl <em>Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nizkov2021.ufois.impl.EventImpl
	 * @see nizkov2021.ufois.impl.UfoisPackageImpl#getEvent()
	 * @generated
	 */
	int EVENT = 5;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__TYPE = 0;

	/**
	 * The feature id for the '<em><b>Author</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__AUTHOR = 1;

	/**
	 * The feature id for the '<em><b>Assistant</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__ASSISTANT = 2;

	/**
	 * The number of structural features of the '<em>Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link nizkov2021.ufois.EEventType <em>EEvent Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nizkov2021.ufois.EEventType
	 * @see nizkov2021.ufois.impl.UfoisPackageImpl#getEEventType()
	 * @generated
	 */
	int EEVENT_TYPE = 6;

	/**
	 * The meta object id for the '{@link nizkov2021.ufois.ESex <em>ESex</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see nizkov2021.ufois.ESex
	 * @see nizkov2021.ufois.impl.UfoisPackageImpl#getESex()
	 * @generated
	 */
	int ESEX = 7;


	/**
	 * The meta object id for the '<em>EBirthday</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.time.LocalDate
	 * @see nizkov2021.ufois.impl.UfoisPackageImpl#getEBirthday()
	 * @generated
	 */
	int EBIRTHDAY = 8;


	/**
	 * Returns the meta object for class '{@link nizkov2021.ufois.Tournament <em>Tournament</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Tournament</em>'.
	 * @see nizkov2021.ufois.Tournament
	 * @generated
	 */
	EClass getTournament();

	/**
	 * Returns the meta object for the containment reference list '{@link nizkov2021.ufois.Tournament#getTeams <em>Teams</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Teams</em>'.
	 * @see nizkov2021.ufois.Tournament#getTeams()
	 * @see #getTournament()
	 * @generated
	 */
	EReference getTournament_Teams();

	/**
	 * Returns the meta object for the containment reference list '{@link nizkov2021.ufois.Tournament#getMatches <em>Matches</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Matches</em>'.
	 * @see nizkov2021.ufois.Tournament#getMatches()
	 * @see #getTournament()
	 * @generated
	 */
	EReference getTournament_Matches();

	/**
	 * Returns the meta object for the attribute '{@link nizkov2021.ufois.Tournament#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see nizkov2021.ufois.Tournament#getName()
	 * @see #getTournament()
	 * @generated
	 */
	EAttribute getTournament_Name();

	/**
	 * Returns the meta object for the '{@link nizkov2021.ufois.Tournament#addEventToMatch(nizkov2021.ufois.Match, nizkov2021.ufois.EEventType, nizkov2021.ufois.Player, nizkov2021.ufois.Player) <em>Add Event To Match</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Event To Match</em>' operation.
	 * @see nizkov2021.ufois.Tournament#addEventToMatch(nizkov2021.ufois.Match, nizkov2021.ufois.EEventType, nizkov2021.ufois.Player, nizkov2021.ufois.Player)
	 * @generated
	 */
	EOperation getTournament__AddEventToMatch__Match_EEventType_Player_Player();

	/**
	 * Returns the meta object for class '{@link nizkov2021.ufois.Player <em>Player</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Player</em>'.
	 * @see nizkov2021.ufois.Player
	 * @generated
	 */
	EClass getPlayer();

	/**
	 * Returns the meta object for the attribute '{@link nizkov2021.ufois.Player#getFirstname <em>Firstname</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Firstname</em>'.
	 * @see nizkov2021.ufois.Player#getFirstname()
	 * @see #getPlayer()
	 * @generated
	 */
	EAttribute getPlayer_Firstname();

	/**
	 * Returns the meta object for the attribute '{@link nizkov2021.ufois.Player#getSurname <em>Surname</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Surname</em>'.
	 * @see nizkov2021.ufois.Player#getSurname()
	 * @see #getPlayer()
	 * @generated
	 */
	EAttribute getPlayer_Surname();

	/**
	 * Returns the meta object for the attribute '{@link nizkov2021.ufois.Player#getNickname <em>Nickname</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Nickname</em>'.
	 * @see nizkov2021.ufois.Player#getNickname()
	 * @see #getPlayer()
	 * @generated
	 */
	EAttribute getPlayer_Nickname();

	/**
	 * Returns the meta object for the attribute '{@link nizkov2021.ufois.Player#getAge <em>Age</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Age</em>'.
	 * @see nizkov2021.ufois.Player#getAge()
	 * @see #getPlayer()
	 * @generated
	 */
	EAttribute getPlayer_Age();

	/**
	 * Returns the meta object for the container reference '{@link nizkov2021.ufois.Player#getTeam <em>Team</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Team</em>'.
	 * @see nizkov2021.ufois.Player#getTeam()
	 * @see #getPlayer()
	 * @generated
	 */
	EReference getPlayer_Team();

	/**
	 * Returns the meta object for the attribute '{@link nizkov2021.ufois.Player#getSex <em>Sex</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Sex</em>'.
	 * @see nizkov2021.ufois.Player#getSex()
	 * @see #getPlayer()
	 * @generated
	 */
	EAttribute getPlayer_Sex();

	/**
	 * Returns the meta object for the reference list '{@link nizkov2021.ufois.Player#getEvents_author <em>Events author</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Events author</em>'.
	 * @see nizkov2021.ufois.Player#getEvents_author()
	 * @see #getPlayer()
	 * @generated
	 */
	EReference getPlayer_Events_author();

	/**
	 * Returns the meta object for the reference list '{@link nizkov2021.ufois.Player#getEvents_assistant <em>Events assistant</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Events assistant</em>'.
	 * @see nizkov2021.ufois.Player#getEvents_assistant()
	 * @see #getPlayer()
	 * @generated
	 */
	EReference getPlayer_Events_assistant();

	/**
	 * Returns the meta object for the attribute '{@link nizkov2021.ufois.Player#getBirthday <em>Birthday</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Birthday</em>'.
	 * @see nizkov2021.ufois.Player#getBirthday()
	 * @see #getPlayer()
	 * @generated
	 */
	EAttribute getPlayer_Birthday();

	/**
	 * Returns the meta object for class '{@link nizkov2021.ufois.Team <em>Team</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Team</em>'.
	 * @see nizkov2021.ufois.Team
	 * @generated
	 */
	EClass getTeam();

	/**
	 * Returns the meta object for the attribute '{@link nizkov2021.ufois.Team#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see nizkov2021.ufois.Team#getName()
	 * @see #getTeam()
	 * @generated
	 */
	EAttribute getTeam_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link nizkov2021.ufois.Team#getPlayers <em>Players</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Players</em>'.
	 * @see nizkov2021.ufois.Team#getPlayers()
	 * @see #getTeam()
	 * @generated
	 */
	EReference getTeam_Players();

	/**
	 * Returns the meta object for the container reference '{@link nizkov2021.ufois.Team#getTournament <em>Tournament</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Tournament</em>'.
	 * @see nizkov2021.ufois.Team#getTournament()
	 * @see #getTeam()
	 * @generated
	 */
	EReference getTeam_Tournament();

	/**
	 * Returns the meta object for class '{@link nizkov2021.ufois.Match <em>Match</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Match</em>'.
	 * @see nizkov2021.ufois.Match
	 * @generated
	 */
	EClass getMatch();

	/**
	 * Returns the meta object for the reference '{@link nizkov2021.ufois.Match#getTeam_a <em>Team a</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Team a</em>'.
	 * @see nizkov2021.ufois.Match#getTeam_a()
	 * @see #getMatch()
	 * @generated
	 */
	EReference getMatch_Team_a();

	/**
	 * Returns the meta object for the reference '{@link nizkov2021.ufois.Match#getTeam_b <em>Team b</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Team b</em>'.
	 * @see nizkov2021.ufois.Match#getTeam_b()
	 * @see #getMatch()
	 * @generated
	 */
	EReference getMatch_Team_b();

	/**
	 * Returns the meta object for the attribute '{@link nizkov2021.ufois.Match#getMatch_id <em>Match id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Match id</em>'.
	 * @see nizkov2021.ufois.Match#getMatch_id()
	 * @see #getMatch()
	 * @generated
	 */
	EAttribute getMatch_Match_id();

	/**
	 * Returns the meta object for the attribute '{@link nizkov2021.ufois.Match#getScore <em>Score</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Score</em>'.
	 * @see nizkov2021.ufois.Match#getScore()
	 * @see #getMatch()
	 * @generated
	 */
	EAttribute getMatch_Score();

	/**
	 * Returns the meta object for the containment reference list '{@link nizkov2021.ufois.Match#getEvents <em>Events</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Events</em>'.
	 * @see nizkov2021.ufois.Match#getEvents()
	 * @see #getMatch()
	 * @generated
	 */
	EReference getMatch_Events();

	/**
	 * Returns the meta object for class '{@link nizkov2021.ufois.UfoIS <em>Ufo IS</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ufo IS</em>'.
	 * @see nizkov2021.ufois.UfoIS
	 * @generated
	 */
	EClass getUfoIS();

	/**
	 * Returns the meta object for the containment reference list '{@link nizkov2021.ufois.UfoIS#getTournaments <em>Tournaments</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Tournaments</em>'.
	 * @see nizkov2021.ufois.UfoIS#getTournaments()
	 * @see #getUfoIS()
	 * @generated
	 */
	EReference getUfoIS_Tournaments();

	/**
	 * Returns the meta object for class '{@link nizkov2021.ufois.Event <em>Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Event</em>'.
	 * @see nizkov2021.ufois.Event
	 * @generated
	 */
	EClass getEvent();

	/**
	 * Returns the meta object for the attribute '{@link nizkov2021.ufois.Event#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see nizkov2021.ufois.Event#getType()
	 * @see #getEvent()
	 * @generated
	 */
	EAttribute getEvent_Type();

	/**
	 * Returns the meta object for the reference '{@link nizkov2021.ufois.Event#getAuthor <em>Author</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Author</em>'.
	 * @see nizkov2021.ufois.Event#getAuthor()
	 * @see #getEvent()
	 * @generated
	 */
	EReference getEvent_Author();

	/**
	 * Returns the meta object for the reference '{@link nizkov2021.ufois.Event#getAssistant <em>Assistant</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Assistant</em>'.
	 * @see nizkov2021.ufois.Event#getAssistant()
	 * @see #getEvent()
	 * @generated
	 */
	EReference getEvent_Assistant();

	/**
	 * Returns the meta object for enum '{@link nizkov2021.ufois.EEventType <em>EEvent Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>EEvent Type</em>'.
	 * @see nizkov2021.ufois.EEventType
	 * @generated
	 */
	EEnum getEEventType();

	/**
	 * Returns the meta object for enum '{@link nizkov2021.ufois.ESex <em>ESex</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>ESex</em>'.
	 * @see nizkov2021.ufois.ESex
	 * @generated
	 */
	EEnum getESex();

	/**
	 * Returns the meta object for data type '{@link java.time.LocalDate <em>EBirthday</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>EBirthday</em>'.
	 * @see java.time.LocalDate
	 * @model instanceClass="java.time.LocalDate"
	 * @generated
	 */
	EDataType getEBirthday();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	UfoisFactory getUfoisFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link nizkov2021.ufois.impl.TournamentImpl <em>Tournament</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see nizkov2021.ufois.impl.TournamentImpl
		 * @see nizkov2021.ufois.impl.UfoisPackageImpl#getTournament()
		 * @generated
		 */
		EClass TOURNAMENT = eINSTANCE.getTournament();

		/**
		 * The meta object literal for the '<em><b>Teams</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TOURNAMENT__TEAMS = eINSTANCE.getTournament_Teams();

		/**
		 * The meta object literal for the '<em><b>Matches</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TOURNAMENT__MATCHES = eINSTANCE.getTournament_Matches();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TOURNAMENT__NAME = eINSTANCE.getTournament_Name();

		/**
		 * The meta object literal for the '<em><b>Add Event To Match</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation TOURNAMENT___ADD_EVENT_TO_MATCH__MATCH_EEVENTTYPE_PLAYER_PLAYER = eINSTANCE.getTournament__AddEventToMatch__Match_EEventType_Player_Player();

		/**
		 * The meta object literal for the '{@link nizkov2021.ufois.impl.PlayerImpl <em>Player</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see nizkov2021.ufois.impl.PlayerImpl
		 * @see nizkov2021.ufois.impl.UfoisPackageImpl#getPlayer()
		 * @generated
		 */
		EClass PLAYER = eINSTANCE.getPlayer();

		/**
		 * The meta object literal for the '<em><b>Firstname</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PLAYER__FIRSTNAME = eINSTANCE.getPlayer_Firstname();

		/**
		 * The meta object literal for the '<em><b>Surname</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PLAYER__SURNAME = eINSTANCE.getPlayer_Surname();

		/**
		 * The meta object literal for the '<em><b>Nickname</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PLAYER__NICKNAME = eINSTANCE.getPlayer_Nickname();

		/**
		 * The meta object literal for the '<em><b>Age</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PLAYER__AGE = eINSTANCE.getPlayer_Age();

		/**
		 * The meta object literal for the '<em><b>Team</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PLAYER__TEAM = eINSTANCE.getPlayer_Team();

		/**
		 * The meta object literal for the '<em><b>Sex</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PLAYER__SEX = eINSTANCE.getPlayer_Sex();

		/**
		 * The meta object literal for the '<em><b>Events author</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PLAYER__EVENTS_AUTHOR = eINSTANCE.getPlayer_Events_author();

		/**
		 * The meta object literal for the '<em><b>Events assistant</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PLAYER__EVENTS_ASSISTANT = eINSTANCE.getPlayer_Events_assistant();

		/**
		 * The meta object literal for the '<em><b>Birthday</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PLAYER__BIRTHDAY = eINSTANCE.getPlayer_Birthday();

		/**
		 * The meta object literal for the '{@link nizkov2021.ufois.impl.TeamImpl <em>Team</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see nizkov2021.ufois.impl.TeamImpl
		 * @see nizkov2021.ufois.impl.UfoisPackageImpl#getTeam()
		 * @generated
		 */
		EClass TEAM = eINSTANCE.getTeam();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TEAM__NAME = eINSTANCE.getTeam_Name();

		/**
		 * The meta object literal for the '<em><b>Players</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TEAM__PLAYERS = eINSTANCE.getTeam_Players();

		/**
		 * The meta object literal for the '<em><b>Tournament</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TEAM__TOURNAMENT = eINSTANCE.getTeam_Tournament();

		/**
		 * The meta object literal for the '{@link nizkov2021.ufois.impl.MatchImpl <em>Match</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see nizkov2021.ufois.impl.MatchImpl
		 * @see nizkov2021.ufois.impl.UfoisPackageImpl#getMatch()
		 * @generated
		 */
		EClass MATCH = eINSTANCE.getMatch();

		/**
		 * The meta object literal for the '<em><b>Team a</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATCH__TEAM_A = eINSTANCE.getMatch_Team_a();

		/**
		 * The meta object literal for the '<em><b>Team b</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATCH__TEAM_B = eINSTANCE.getMatch_Team_b();

		/**
		 * The meta object literal for the '<em><b>Match id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATCH__MATCH_ID = eINSTANCE.getMatch_Match_id();

		/**
		 * The meta object literal for the '<em><b>Score</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATCH__SCORE = eINSTANCE.getMatch_Score();

		/**
		 * The meta object literal for the '<em><b>Events</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATCH__EVENTS = eINSTANCE.getMatch_Events();

		/**
		 * The meta object literal for the '{@link nizkov2021.ufois.impl.UfoISImpl <em>Ufo IS</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see nizkov2021.ufois.impl.UfoISImpl
		 * @see nizkov2021.ufois.impl.UfoisPackageImpl#getUfoIS()
		 * @generated
		 */
		EClass UFO_IS = eINSTANCE.getUfoIS();

		/**
		 * The meta object literal for the '<em><b>Tournaments</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UFO_IS__TOURNAMENTS = eINSTANCE.getUfoIS_Tournaments();

		/**
		 * The meta object literal for the '{@link nizkov2021.ufois.impl.EventImpl <em>Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see nizkov2021.ufois.impl.EventImpl
		 * @see nizkov2021.ufois.impl.UfoisPackageImpl#getEvent()
		 * @generated
		 */
		EClass EVENT = eINSTANCE.getEvent();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EVENT__TYPE = eINSTANCE.getEvent_Type();

		/**
		 * The meta object literal for the '<em><b>Author</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EVENT__AUTHOR = eINSTANCE.getEvent_Author();

		/**
		 * The meta object literal for the '<em><b>Assistant</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EVENT__ASSISTANT = eINSTANCE.getEvent_Assistant();

		/**
		 * The meta object literal for the '{@link nizkov2021.ufois.EEventType <em>EEvent Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see nizkov2021.ufois.EEventType
		 * @see nizkov2021.ufois.impl.UfoisPackageImpl#getEEventType()
		 * @generated
		 */
		EEnum EEVENT_TYPE = eINSTANCE.getEEventType();

		/**
		 * The meta object literal for the '{@link nizkov2021.ufois.ESex <em>ESex</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see nizkov2021.ufois.ESex
		 * @see nizkov2021.ufois.impl.UfoisPackageImpl#getESex()
		 * @generated
		 */
		EEnum ESEX = eINSTANCE.getESex();

		/**
		 * The meta object literal for the '<em>EBirthday</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.time.LocalDate
		 * @see nizkov2021.ufois.impl.UfoisPackageImpl#getEBirthday()
		 * @generated
		 */
		EDataType EBIRTHDAY = eINSTANCE.getEBirthday();

	}

} //UfoisPackage
