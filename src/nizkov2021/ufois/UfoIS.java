/**
 */
package nizkov2021.ufois;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ufo IS</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link nizkov2021.ufois.UfoIS#getTournaments <em>Tournaments</em>}</li>
 * </ul>
 *
 * @see nizkov2021.ufois.UfoisPackage#getUfoIS()
 * @model
 * @generated
 */
public interface UfoIS extends EObject {
	/**
	 * Returns the value of the '<em><b>Tournaments</b></em>' containment reference list.
	 * The list contents are of type {@link nizkov2021.ufois.Tournament}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tournaments</em>' containment reference list.
	 * @see nizkov2021.ufois.UfoisPackage#getUfoIS_Tournaments()
	 * @model containment="true"
	 * @generated
	 */
	EList<Tournament> getTournaments();

} // UfoIS
